# Simple Video Kiosk

This Cordova app for Android simply loops an html5 video in full screen mode, with no controls. It is used to show a video on screen, and prevent anyone from messing with it (ie, stop it, change the layout). Some preferences are set in config.xml to  further reinforce the kiosk mode. Currently the video is located in www/video folder, so it must be present when compiling.  You'll also find samples there, that were used in the Telus Connnected Playground Installation.

Nick Stedman
Summer 2016
nick@robotstack.com

The MIT License (MIT)
Copyright (c) <2016> <Nicholas Stedman>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


